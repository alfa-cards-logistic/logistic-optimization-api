package ru.nkrus.api.logistic.service.optimization;

import org.junit.Before;
import org.junit.Test;
import ru.nkrus.api.logistic.domain.DataModel;

import java.util.List;

public class CourierSolverTest {

    @Before
    public void setUp() throws Exception {
        System.loadLibrary("jniortools");
    }

    @Test
    public void solve() {
        CourierSolver solver = new CourierSolver();
        //solver.solve(dataModelVenera());
    }

    private DataModel dataModelVenera() {
        return DataModel.builder()
                .timeMatrix(
                        new int[][]{
                                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                                {0, 0, 56, 39, 74, 56, 55, 52, 42},
                                {0, 54, 0, 38, 77, 0, 27, 25, 41},
                                {0, 38, 50, 0, 63, 50, 34, 33, 52},
                                {0, 75, 91, 67, 0, 91, 80, 82, 91},
                                {0, 54, 0, 38, 77, 0, 27, 25, 41},
                                {0, 53, 28, 34, 74, 28, 0, 28, 27},
                                {0, 52, 17, 34, 71, 17, 27, 0, 43},
                                {0, 43, 53, 52, 77, 53, 27, 43, 0}

                        })
                .timeWindows(new int[][]{
                        {0, 1440},
                        {720, 720},
                        {960, 960},
                        {780, 780},
                        {660, 660},
                        {960, 960},
                        {720, 720},
                        {840, 840},
                        {960, 960}
                })
                .serviceTime(List.of(20,20,20,20,20,20,20,20))
                .couriersNumber(3)
                .depot(0)
                .build();
    }
}