package ru.nkrus.api.logistic.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.jni.Library;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class NativeConfig {
    static {
        log.info("Loading jniortools library");
        Library.loadLibrary("jniortools");
    }
}
