package ru.nkrus.api.logistic.config;

import com.google.maps.GeoApiContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GoogleApiConfig {

    @Bean
    public GeoApiContext geoApiContext(@Value("${google.api.key}") String apiKey){
        return new GeoApiContext.Builder()
                .apiKey(apiKey)
                .disableRetries()
                .queryRateLimit(1)
                .build();
    }
}
