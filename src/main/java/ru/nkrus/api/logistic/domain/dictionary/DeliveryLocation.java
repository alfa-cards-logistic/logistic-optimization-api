package ru.nkrus.api.logistic.domain.dictionary;

import lombok.Data;
import lombok.EqualsAndHashCode;
import ru.nkrus.api.logistic.validation.annotations.CardsOrDurationExists;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.LocalTime;

@Data
@EqualsAndHashCode(callSuper = true)
@CardsOrDurationExists
public class DeliveryLocation extends Location{

    @NotNull(message = "Время раннего визита не может быть пустым")
    private LocalTime visitTime;
    @NotNull(message = "Время позднего визита не может быть пустым")
    private LocalTime lateVisitTime;
    @Min(value = 0, message = "Количество карт не может быть меньше нуля")
    private Integer cardsAmount;

    private Duration duration; //TODO добавить конвертер
}
