package ru.nkrus.api.logistic.domain;

import lombok.Data;
import ru.nkrus.api.logistic.domain.dictionary.DeliveryLocation;
import ru.nkrus.api.logistic.domain.dictionary.Employee;
import ru.nkrus.api.logistic.domain.dictionary.Location;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class OptimizationRequest {

    @NotNull(message = "Адрес места отправления не может быть пустым")
    private Location depotLocation;
    @NotEmpty(message = "Должен присутствовать хотя бы один сотрудник в наличии")
    private List<Employee> employees;
    @NotEmpty(message = "Должна присутствовать хотя бы одна точка доставки")
    private List<DeliveryLocation> deliveryLocations;

}
