package ru.nkrus.api.logistic.domain.dictionary;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class Employee {

    @NotEmpty(message = "Фамилия сотрудника не может быть пустая")
    private String surname;
    @NotEmpty(message = "Имя сотрудника не может быть пустым")
    private String firstname;

    private String middlename;


    public String getFullName() {
        return String.join(" ", surname, firstname, middlename);
    }
}
