package ru.nkrus.api.logistic.domain.dictionary;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class Location {

    @NotEmpty
    private String address;


}
