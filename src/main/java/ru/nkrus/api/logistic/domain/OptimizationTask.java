package ru.nkrus.api.logistic.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OptimizationTask {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;
    private Status status;
    private OptimizationRequest optimizationRequest;

    public enum  Status {
        NEW, PROCESSING, COMPLETED, ERROR
    }
}
