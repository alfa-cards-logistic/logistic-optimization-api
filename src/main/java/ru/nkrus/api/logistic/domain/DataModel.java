package ru.nkrus.api.logistic.domain;

import lombok.Builder;
import lombok.Value;

import java.util.Arrays;
import java.util.List;

@Value
@Builder
public class DataModel {
    private final int[][] timeMatrix;
    private final int[][] timeWindows;
    private final List<Integer> serviceTime;
    private final int couriersNumber;
    private final int depot;

    @Override
    public String toString() {
        return "DataModel{" +
                "timeMatrix=" + Arrays.deepToString(timeMatrix) +
                ", timeWindows=" + Arrays.deepToString(timeWindows) +
                ", serviceTime=" + serviceTime +
                ", couriersNumber=" + couriersNumber +
                ", depot=" + depot +
                '}';
    }
}
