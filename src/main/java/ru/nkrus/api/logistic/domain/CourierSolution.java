package ru.nkrus.api.logistic.domain;


import lombok.Builder;
import lombok.Value;
import ru.nkrus.api.logistic.domain.dictionary.DeliveryLocation;

import java.time.LocalDateTime;
import java.util.List;

@Value
@Builder
public class CourierSolution {

    private final LocalDateTime solutionDate;
    private final List<Solution> solution;

    @Value
    @Builder
    public static class Solution {
        private final String name;
        private final List<DeliveryLocation> locations;
    }
}
