package ru.nkrus.api.logistic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class LogisticApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogisticApiApplication.class, args);
    }

}
