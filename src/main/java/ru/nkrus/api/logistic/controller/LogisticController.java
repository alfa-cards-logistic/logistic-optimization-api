package ru.nkrus.api.logistic.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nkrus.api.logistic.domain.OptimizationRequest;
import ru.nkrus.api.logistic.service.TaskService;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LogisticController {

    private final TaskService taskService;

    @PostMapping("/tasks")
    public ResponseEntity<?> createTask(@RequestBody @Valid OptimizationRequest request) {
        log.info("Requesting optimization task: {}", request);
        return new ResponseEntity<>(taskService.createOptimizationTask(request), HttpStatus.OK);
    }

    @GetMapping("/tasks")
    public ResponseEntity<?> tasks(){
        log.info("Requesting all tasks");
        return new ResponseEntity<>(taskService.optimizationRequests(), HttpStatus.OK);
    }
}
