package ru.nkrus.api.logistic.converter;

import com.google.maps.model.DistanceMatrix;
import org.apache.commons.lang3.ArrayUtils;
import ru.nkrus.api.logistic.domain.DataModel;
import ru.nkrus.api.logistic.domain.OptimizationRequest;
import ru.nkrus.api.logistic.domain.dictionary.DeliveryLocation;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataModelConverter {

    private static final Integer MAX_CARD_SERVICE_TIME = 480;

    private static final Map<Integer, Integer> serviceTimeByCardsAmount = Map.of(10, 20, 20, 50, 30, 100, 50, 180);

    /**
     * Конвертирует входные данные пользователя и матрицу расстояний в оптимизационную модель
     * для дальнейшего решения задачи
     *
     * @param optimizationRequest запрос с входными данными для оптимизации
     * @param matrix              матрица расстояний из Google API
     * @return модель данных для оптимизации
     */
    public static DataModel convert(OptimizationRequest optimizationRequest, DistanceMatrix matrix) {

        int[][] timeMatrix = getTimeMatrix(matrix);
        int[][] timeWindows = getTimeWindows(optimizationRequest);

        return DataModel.builder()
                .timeMatrix(matrixWithZeroDepot(timeMatrix))
                .timeWindows(timeWindows)
                .serviceTime(serviceTime(optimizationRequest))
                .couriersNumber(optimizationRequest.getEmployees().size())
                .depot(0)
                .build();
    }

    /**
     * Получение матрицы времени
     *
     * @param distanceMatrix матрица расстояний из Google API
     * @return матрица времени
     */
    private static int[][] getTimeMatrix(DistanceMatrix distanceMatrix) {
        int[][] matrix = new int[distanceMatrix.originAddresses.length][distanceMatrix.destinationAddresses.length];
        for (int i = 0; i < distanceMatrix.rows.length; i++) {
            for (int j = 0; j < distanceMatrix.rows[i].elements.length; j++) {
                matrix[i][j] = Math.toIntExact(distanceMatrix.rows[i].elements[j].duration.inSeconds / 60);
            }
        }
        return matrix;
    }

    /**
     * Получение матрицы временных ограничений
     *
     * @param optimizationRequest запрос с входными данными для оптимизации
     * @return матрица временных ограничений
     */
    private static int[][] getTimeWindows(OptimizationRequest optimizationRequest) {

        int[][] matrix = new int[optimizationRequest.getDeliveryLocations().size() + 1][2];

        //Depot not using in time constraints
        matrix[0][0] = matrix[0][1] = 0;

        for (int i = 0; i < optimizationRequest.getDeliveryLocations().size(); i++) {
            matrix[i + 1][0] = optimizationRequest.getDeliveryLocations().get(i).getVisitTime().toSecondOfDay() / 60;
            matrix[i + 1][1] = optimizationRequest.getDeliveryLocations().get(i).getLateVisitTime().toSecondOfDay() / 60;
        }
        return matrix;
    }

    /**
     * Добавляет в матрицу времени dummy depot(строка с нулевым временем)
     * Так как все курьеры возвращаются в исходную точку после решения задачи,
     * dummy depot позволяет принять что точка отправления динамическая и курьеры
     * отправляются каждый из случайной локации
     *
     * @param matrix матрица времени
     * @return матрица времени с dummy depot
     */
    private static int[][] matrixWithZeroDepot(int[][] matrix) {
        int[][] withDepot = new int[matrix.length + 1][matrix[0].length + 1];
        for (int i = 0; i < matrix.length; i++) {
            withDepot[i + 1] = ArrayUtils.addAll(new int[]{0}, matrix[i]);
        }
        return withDepot;
    }

    private static List<Integer> serviceTime(OptimizationRequest optimizationRequest) {
        return optimizationRequest.getDeliveryLocations().stream()
                .map(DeliveryLocation::getCardsAmount)
                .map(a -> serviceTimeByCardsAmount.entrySet().stream()
                        .filter(e -> a < e.getKey())
                        .map(Map.Entry::getValue)
                        .sorted()
                        .findFirst()
                        .orElse(MAX_CARD_SERVICE_TIME))
                .collect(Collectors.toList());


    }
}
