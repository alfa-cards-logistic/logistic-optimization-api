package ru.nkrus.api.logistic.converter;

import com.google.ortools.constraintsolver.*;
import ru.nkrus.api.logistic.domain.CourierSolution;
import ru.nkrus.api.logistic.domain.DataModel;
import ru.nkrus.api.logistic.domain.OptimizationTask;
import ru.nkrus.api.logistic.domain.dictionary.DeliveryLocation;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CourierSolutionConverter {

    public static CourierSolution convert(OptimizationTask request, DataModel data, RoutingModel routing,
                                          RoutingIndexManager manager, Assignment solution) {
        List<CourierSolution.Solution> solutions = new ArrayList<>();

        for (int i = 0; i < data.getCouriersNumber(); ++i) {
            long index = solution.value(routing.nextVar(routing.start(i)));
            List<DeliveryLocation> locations = new ArrayList<>();
            while (!routing.isEnd(index)) {
                //Добавляем локацию в список курьера
                locations.add(request.getOptimizationRequest().getDeliveryLocations().get(manager.indexToNode(index) - 1));
                index = solution.value(routing.nextVar(index));
            }

            solutions.add(CourierSolution.Solution.builder()
                    .name(request.getOptimizationRequest().getEmployees().get(i).getFullName())
                    .locations(locations)
                    .build());
        }

        return CourierSolution.builder()
                .solution(solutions)
                .solutionDate(LocalDateTime.now())
                .build();
    }
}
