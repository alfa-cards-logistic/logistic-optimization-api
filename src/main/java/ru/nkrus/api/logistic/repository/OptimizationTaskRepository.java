package ru.nkrus.api.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.nkrus.api.logistic.domain.OptimizationTask;

import java.util.List;

public interface OptimizationTaskRepository extends MongoRepository<OptimizationTask, String> {

    List<OptimizationTask> findByStatus(OptimizationTask.Status status);

}
