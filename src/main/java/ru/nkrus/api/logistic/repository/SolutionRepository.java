package ru.nkrus.api.logistic.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.nkrus.api.logistic.domain.CourierSolution;

public interface SolutionRepository extends MongoRepository<CourierSolution, String> {
}
