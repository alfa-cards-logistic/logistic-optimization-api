package ru.nkrus.api.logistic.service.optimization;

import com.google.ortools.constraintsolver.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.nkrus.api.logistic.converter.CourierSolutionConverter;
import ru.nkrus.api.logistic.domain.CourierSolution;
import ru.nkrus.api.logistic.domain.DataModel;
import ru.nkrus.api.logistic.domain.OptimizationTask;

@Slf4j
@Component
public class CourierSolver {

    public CourierSolution solve(OptimizationTask task, DataModel data) {

        // Create Routing Index Manager
        RoutingIndexManager manager = new RoutingIndexManager(data.getTimeMatrix().length,
                data.getCouriersNumber(), data.getDepot());

        // Create Routing Model.
        RoutingModel routing = new RoutingModel(manager);

        final int transitCallbackIndex =
                routing.registerTransitCallback((long fromIndex, long toIndex) -> {
                    // Convert from routing variable Index to user NodeIndex.
                    int fromNode = manager.indexToNode(fromIndex);
                    int toNode = manager.indexToNode(toIndex);
                    //Если точка не depot(0) добавляем время обслуживания к transit time
                    return toNode == 0 ? data.getTimeMatrix()[fromNode][toNode]
                            : (data.getTimeMatrix()[fromNode][toNode] + data.getServiceTime().get(toNode - 1));
                });
        routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);

        routing.addDimension(transitCallbackIndex, // transit callback
                1000, // allow waiting time
                1000, // vehicle maximum capacities
                true, // start cumul to zero
                "Time");
        RoutingDimension timeDimension = routing.getMutableDimension("Time");
        // Add time window constraints for each location except depot.
        for (int i = 1; i < data.getTimeWindows().length; ++i) {
            long index = manager.nodeToIndex(i);
            timeDimension.cumulVar(index).setMin(data.getTimeWindows()[i][0]);
            timeDimension.cumulVar(index).setMax(data.getTimeWindows()[i][1]);
        }
        // Add time window constraints for each vehicle start node.
        for (int i = 0; i < data.getCouriersNumber(); ++i) {
            long index = routing.start(i);
            timeDimension.cumulVar(index).setRange(data.getTimeWindows()[0][0], data.getTimeWindows()[0][1]);
        }
        for (int i = 0; i < data.getCouriersNumber(); ++i) {
            routing.addVariableMinimizedByFinalizer(timeDimension.cumulVar(routing.start(i)));
            routing.addVariableMinimizedByFinalizer(timeDimension.cumulVar(routing.end(i)));
        }
        RoutingSearchParameters searchParameters =
                main.defaultRoutingSearchParameters()
                        .toBuilder()
                        .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
                        .setLogSearch(true)
                        .build();
        Assignment solution = routing.solveWithParameters(searchParameters);

        printSolution(data, routing, manager, solution);

        return CourierSolutionConverter.convert(task, data, routing, manager, solution);

    }

    /// @brief Print the solution.
    static void printSolution(
            DataModel data, RoutingModel routing, RoutingIndexManager manager, Assignment solution) {
        RoutingDimension timeDimension = routing.getMutableDimension("Time");
        long totalTime = 0;
        for (int i = 0; i < data.getCouriersNumber(); ++i) {
            long index = routing.start(i);
            log.info("Route for Vehicle " + i + ":");
            String route = "";
            while (!routing.isEnd(index)) {
                IntVar timeVar = timeDimension.cumulVar(index);
                route += manager.indexToNode(index) + " Time(" + solution.min(timeVar) + ","
                        + solution.max(timeVar) + ") -> ";
                index = solution.value(routing.nextVar(index));
            }
            IntVar timeVar = timeDimension.cumulVar(index);
            route += manager.indexToNode(index) + " Time(" + solution.min(timeVar) + ","
                    + solution.max(timeVar) + ")";
            log.info(route);
            log.info("Time of the route: " + solution.min(timeVar) + "min");
            totalTime += solution.min(timeVar);
        }
        log.info("Total time of all routes: " + totalTime + "min");
    }
}
