package ru.nkrus.api.logistic.service.api;

import com.google.maps.DistanceMatrixApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.TransitMode;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nkrus.api.logistic.domain.dictionary.DeliveryLocation;
import ru.nkrus.api.logistic.domain.dictionary.Location;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DistanceMatrixService {

    private final GeoApiContext geoApiContext;

    public DistanceMatrix distanceMatrix(List<DeliveryLocation> locations) throws InterruptedException, ApiException, IOException {
        String[] addresses = locations.stream()
                .map(Location::getAddress)
                .toArray(String[]::new);

        log.info("Requesting distance matrix for Locations: {}", Arrays.toString(addresses));

        return DistanceMatrixApi.newRequest(geoApiContext)
                .mode(TravelMode.TRANSIT)
                .transitModes(TransitMode.RAIL, TransitMode.BUS)
                .origins(addresses)
                .destinations(addresses)
                .units(Unit.METRIC)
                .language("ru")
                .await();
    }
}
