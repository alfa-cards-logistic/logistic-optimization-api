package ru.nkrus.api.logistic.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.nkrus.api.logistic.domain.OptimizationRequest;
import ru.nkrus.api.logistic.domain.OptimizationTask;
import ru.nkrus.api.logistic.repository.OptimizationTaskRepository;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TaskService {

    private final OptimizationTaskRepository optimizationTaskRepository;

    public String createOptimizationTask(OptimizationRequest request){
        OptimizationTask task = OptimizationTask.builder()
                .status(OptimizationTask.Status.NEW)
                .optimizationRequest(request)
                .build();
        OptimizationTask inserted = optimizationTaskRepository.insert(task);
        return inserted.getId();
    }

    public List<OptimizationTask> optimizationRequests(){
        return optimizationTaskRepository.findAll();
    }
}
