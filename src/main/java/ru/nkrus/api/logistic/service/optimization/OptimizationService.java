package ru.nkrus.api.logistic.service.optimization;

import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.nkrus.api.logistic.converter.DataModelConverter;
import ru.nkrus.api.logistic.domain.CourierSolution;
import ru.nkrus.api.logistic.domain.DataModel;
import ru.nkrus.api.logistic.domain.OptimizationTask;
import ru.nkrus.api.logistic.repository.OptimizationTaskRepository;
import ru.nkrus.api.logistic.repository.SolutionRepository;
import ru.nkrus.api.logistic.service.api.DistanceMatrixService;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class OptimizationService {

    private final OptimizationTaskRepository optimizationTaskRepository;

    private final SolutionRepository solutionRepository;

    private final DistanceMatrixService matrixService;

    private final CourierSolver courierSolver;

    @Scheduled(fixedRate = 5000)
    public void solveTasks() {
        List<OptimizationTask> newTasks = optimizationTaskRepository.findByStatus(OptimizationTask.Status.NEW);
        log.info("OptimizationTask repository size: {}", newTasks.size());
        if (!newTasks.isEmpty()) {
            OptimizationTask task = newTasks.get(0);
            log.info("Started solving task: {}", task.getId());
            try {
                task.setStatus(OptimizationTask.Status.PROCESSING);
                processTask(task);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                task.setStatus(OptimizationTask.Status.ERROR);
            } finally {
                log.info("Updating task after processing");
                optimizationTaskRepository.save(task);
            }
        }
    }

    private void processTask(OptimizationTask task) throws InterruptedException, ApiException, IOException {
        log.info("Start processing task: {}", task.getId());
        DistanceMatrix matrix = matrixService.distanceMatrix(task.getOptimizationRequest().getDeliveryLocations());
        DataModel data = DataModelConverter.convert(task.getOptimizationRequest(), matrix);
        log.info("Data model: {}", data);
        CourierSolution courierSolution = courierSolver.solve(task, data);
        solutionRepository.insert(courierSolution);
        task.setStatus(OptimizationTask.Status.COMPLETED);
        log.info("Task {} solved", task.getId());
    }
}
