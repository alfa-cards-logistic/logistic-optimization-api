package ru.nkrus.api.logistic.validation.annotations;

import ru.nkrus.api.logistic.validation.validators.CardsOrDurationExistsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Constraint(validatedBy = CardsOrDurationExistsValidator.class)
public @interface CardsOrDurationExists {
    String message() default  "Количество карт для доставки или время потраченное в компании должно быть заполнено";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

