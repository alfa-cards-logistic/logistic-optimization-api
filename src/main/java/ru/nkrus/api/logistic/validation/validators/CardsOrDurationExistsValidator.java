package ru.nkrus.api.logistic.validation.validators;


import ru.nkrus.api.logistic.domain.dictionary.DeliveryLocation;
import ru.nkrus.api.logistic.validation.annotations.CardsOrDurationExists;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CardsOrDurationExistsValidator implements ConstraintValidator<CardsOrDurationExists, DeliveryLocation> {
    @Override
    public boolean isValid(DeliveryLocation value, ConstraintValidatorContext context) {
        return value.getCardsAmount() != null || value.getDuration() != null;
    }
}
